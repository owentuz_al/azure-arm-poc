.phony: all deploy test clean

all: | test deploy

test:
	./scripts/test.sh

deploy:
	./scripts/deploy.sh

clean:
	./scripts/unprovision.sh
