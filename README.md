ARM template PoC
================

A quick project for learning the basics of Azure and ARM templates.

Creates the following resources within Azure:


- A containing resource group
- A VNet with one subnet
- A ScaleSet of Linux instances
- A load balancer and DNS entry
- Inbound and outbound NAT rules allowing the instances to reach the internet, and allowing SSH access via load balancer port forwarding

Improvements to make this more 'production-like':


- More generic test/deploy scripts, these have some hardcoded entries in them
- A real pipeline would use a service principal and credentials in some form of secret storage. We're using manual login here for simplicity
- Drop the resource group template/move into a different pipeline. Handling both cases here is awkward and not really necessary
- Add proper tests (something like awspec but for Azure?)

Requirements
------------
You'll need an Azure account, and the Azure CLI for your OS.
The deploy scripts make use of `jq`.

```
brew install azure-cli jq
```

Usage
-----

- Before using, log into the Azure cli with `az login`
- Test and deploy with `make`.
- `make clean` to destroy all existing resources.
