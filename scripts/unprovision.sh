#!/bin/bash
set -eu

die(){
  echo "$1" >&2
  exit 1
}

cd $(dirname $0)/../

RG_NAME=$(jq -r '.parameters.resourceGroupName.value' parameters.json)

az group delete --name ${RG_NAME}
