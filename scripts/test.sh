#!/bin/bash
set -eu

die(){
  echo "$1" >&2
  exit 1
}

cd $(dirname $0)/../

for JSON_FILE in $(ls *.json); do
    cat "$JSON_FILE" | python -m json.tool >/dev/null || die "${JSON_FILE}: Invalid JSON"
    echo "${JSON_FILE}: is valid JSON"
done

# Validate deployment using default location as in almost all cases this won't
# affect the output

RG_VALIDATE_OUTPUT=$(az deployment validate \
                       --template-file resourceGroup.template.json \
                       --parameters @parameters.json --location uksouth \
                       | jq -r '.error')

if ! [[ "$RG_VALIDATE_OUTPUT" == "null" ]]; then # There was an error
  cat << EOF >&2
${RG_VALIDATE_OUTPUT}
EOF
  die "Failed validating resource group template"
else
  echo "resourceGroup.template.json passed validation"
fi

RG_NAME=$(jq -r '.parameters.resourceGroupName.value' parameters.json)

[[ "$RG_NAME" == "null" ]] && die "Failed to retrieve resourceGroupName from parameters.json"

# If the resource group has not been created, we can't validate the deployment
# so just exit here
az group list --query "[?name=='${RG_NAME}']" | jq -er '.[0].name' || exit 0

DEPLOY_VALIDATE_OUTPUT=$(az group deployment validate \
                           --template-file vnetResources.template.json \
                           --parameters @parameters.json -g ${RG_NAME} \
                           | jq -r '.error')

if ! [[ "$DEPLOY_VALIDATE_OUTPUT" == "null" ]]; then # There was an error
  cat << EOF >&2
${DEPLOY_VALIDATE_OUTPUT}
EOF
else
  echo "vnetResources.template.json passed validation"
fi
