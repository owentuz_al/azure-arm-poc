#!/bin/bash
set -eu

die(){
  echo "$1" >&2
  exit 1
}

cd $(dirname $0)/../

RG_LOCATION=$(jq -er '.parameters.resourceGroupLocation.value // empty' parameters.json \
              || jq -r '.parameters.resourceGroupLocation.defaultValue' resourceGroup.template.json)
RG_NAME=$(jq -r '.parameters.resourceGroupName.value' parameters.json)

[[ "$RG_LOCATION" == "null" ]] && die "Could not retrieve value of required parameter 'location'"
[[ "$RG_NAME" == "null" ]] && die "Failed to retrieve resourceGroupName from parameters.json"

if az group list --query "[?name=='${RG_NAME}']" | jq -er '.[0].name'; then
  echo "Resource group ${RG_NAME} already exists, skipping template"
else
  az deployment create --template-file resourceGroup.template.json \
                       --parameters @parameters.json \
                       --location ${RG_LOCATION}
fi

az group deployment create --template-file vnetResources.template.json \
                           --parameters @parameters.json \
                           --resource-group ${RG_NAME}
